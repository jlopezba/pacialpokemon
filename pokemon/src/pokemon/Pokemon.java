package pokemon;

import java.util.Scanner;

public class Pokemon {

    static Maestro_POKEMON[] aMaestros;
    static int numMaestros;
    static Scanner entrada;
    static int nomPokemon;

    public static void main(String[] args) {

        // creo objeto de mio clase princiopal 
        entrada = new Scanner(System.in);

        System.out.println(" BIENVENIDOS A LA FUERTE BATALLA DE POKEMONES ");

        //declaracion de i arreglo de maestros 
        // tipoDeDATO[] nombre = new TipoDEDATo[6];
        aMaestros = new Maestro_POKEMON[6];
        numMaestros = 0;
        // aMaestros[0] = variale de tipodmaestro 
        // sysstem ( Mastero_pokemon[0])
        // sobrecaraga de contructor 
        // construir el swicth 
        boolean end = false;

        while (!end) {

            System.out.println("\n\n ####################### Menu Principal ####################################### ");
            System.out.println("1. Insertar maestros ");
            System.out.println("2. Mostar maestro ");
            System.out.println("3. Insertar pokemmon  a un maestro ");
            System.out.println("4. Jugar  ");
            System.out.println("5. salir   ");

            System.out.print("Digite una opcion :   ");
            String op = entrada.next();

            switch (op) {

                case "1":
                    recogerDatosDeMaestro();

                case "2":

                    mostrarMaestros();
                    break;

                case "3":
                      mostrarMaestros();                    
                    System.out.print(" \n\n A que e maestro desea insertarl poekmon : ");
                    int aux = entrada.nextInt();
                    crearPokemon(aMaestros[aux]);

                    break;

                case "4":
                    jugar();
                    break;
                case "5":
                    System.out.println("Gracias por jugar");
                    end = true;
                    break;

                default:
                    System.out.println("Digito mal un numero  ");

            }

            // llamo a mis metodos crar y recoger datos.
        }

    }

    public static void recogerDatosDeMaestro() {
        int i = numMaestros;

        if (numMaestros < 6) {
            System.out.print(" POR  FAVOR REGISTRE LOS DATOS DE LOS MAESTRO : ");
            //recibo el nombre y lo almaceno en un variable nombre 
            String nombre = entrada.next();
            // cree mi objeto 
            Maestro_POKEMON maestroTemp = new Maestro_POKEMON();
            // utilizo mi maestro al cual deseo cambiarle el atributo utiliazando mi metodo ser nombre  con el valro nombre 
            maestroTemp.setNombre(nombre);

            aMaestros[i] = maestroTemp;
            numMaestros++;
        } else {
            System.out.println("Y an o hay mas espacio para maestros ");
        }
    }

    public static void mostrarMaestros() {

        // mostsrar mis maestros 
        System.out.println(" \n\n   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ MAESTROS $$$$$$$$$$$$$$$$$$$$$ ");
        for (int i = 0; i < numMaestros; i++) {

            // cree mi objeto 
            Maestro_POKEMON maestroTemp = aMaestros[i];
            // utilizo mi maestro al cual deseo cambiarle el atributo utiliazando mi metodo ser nombre  con el valro nombre 

            System.out.println( " Maestro "+  i + "  : " + maestroTemp.getnNombre());
            

        }

    }
     public static void mostrarMaestroConPokemones() {

        // mostsrar mis maestros 
        System.out.println(" \n\n  |||||||||||||||||||||||||Maestros  con sus pokemon ||||||||||||||||||||||||||||||||||||");
        for (int i = 0; i < numMaestros; i++) {

            // cree mi objeto 
            Maestro_POKEMON maestroTemp = aMaestros[i];
            // utilizo mi maestro al cual deseo cambiarle el atributo utiliazando mi metodo ser nombre  con el valro nombre 

 
            maestroTemp.imprimirPokemonesDisponibles();
        }

    }

    // metodo para alamcenar los datos de pokemon 
    public static void crearPokemon(Maestro_POKEMON m) {

        boolean end = false;

        while (!end) {

            System.out.println("\n\n #########################  Tipo de Pokemon ################################ ");
            System.out.println("1. Es un animal de hielo ");
            System.out.println("2. Es un animal de fuego ");

            String op = entrada.next();

            switch (op) {

                case "1":

                    AnimalPokemon a = new AnimaHielo();
                    System.out.print("Nombre de pokemon :");
                    String no = entrada.next();
                    System.out.print("Ataque:");
                    double at = entrada.nextDouble();
                    System.out.print("Defensa:  ");
                    double de = entrada.nextDouble();

                    // lenado la inormacion del poekemon 
                    a.setNombre(no);
                    a.setAtaque(at);
                    a.setDefensa(de);

                    //asinamos el pokemon 
                    m.aniadirPokemon(a);

                    end = true;
                    break;
                case "2":

                    AnimalPokemon b = new AnimalFuego();
                    System.out.print("Nombre de pokemon :");
                    String no1 = entrada.next();
                    System.out.print("Ataque:");
                    double at1 = entrada.nextDouble();
                    System.out.print("Defensa:  ");
                    double de1 = entrada.nextDouble();

                    // lenado la inormacion del poekemon 
                    b.setNombre(no1);
                    b.setAtaque(at1);
                    b.setDefensa(de1);

                    //asinamos el pokemon 
                    m.aniadirPokemon(b);

                    end = true;
                    break;

                default:
                    System.out.println("Digito mal un numero  ");

            }

            // llamo a mis metodos crar y recoger datos.
        }

    }

    private static AnimalPokemon obtenerPokemonDuelo() {
        mostrarMaestros();
        System.out.print(" \n\n Oponente : ");
        int aux = entrada.nextInt();
        aMaestros[aux].imprimirPokemonesDisponibles();

        System.out.print(" \n\n selecione el pokemon  : ");

        int pos = entrada.nextInt();
        AnimalPokemon temp = aMaestros[aux].getAnimalDeArreglo(pos);

        System.out.println("\n\n ### a sleccionado a " + temp.getNombre());

        return temp;

    }

    static void jugar() {
        System.out.println("/////////////////////////////////////////////Enftrentamiento  ////////////////////////////////////");
        
        AnimalPokemon op1 = obtenerPokemonDuelo(); 
        System.out.println("Contra ");
        AnimalPokemon op2 = obtenerPokemonDuelo(); 
        
        
        
      impmirInfoPokemon(op1);  
      impmirInfoPokemon(op2); 
      
        System.out.println("//// \n eel ganador es ql que tenga ataque /// \n");             
      if(op1.getAtaque()>op2.ataque){
          System.out.println("Gana Pokemon  " + op1.getNombre() );
      }else{
         System.out.println("Gana Pokemon  " + op1.getNombre() );  
      }
      

    }

    static void impmirInfoPokemon(AnimalPokemon a) {
        System.out.println("Nombre " + a.getNombre());
        System.out.println("Vida " + a.getVida());
        System.out.println("Ataque  " + a.getAtaque());
        System.out.println("Ataque  " + a.getDefensa());
    }

    // un metodo para imprimir
    public void inprimirDatos() {

    }

}
